package org.woody.musicplyaer_ver1;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by woody on 2016. 8. 11..
 */
public class ViewHolder extends RecyclerView.ViewHolder {

    ImageView albumView;
    TextView titleView, singerView;

    public ViewHolder(View itemView){
       super(itemView);

        albumView = (ImageView)itemView.findViewById(R.id.albumView);
        titleView = (TextView)itemView.findViewById(R.id.titleView);
        singerView = (TextView)itemView.findViewById(R.id.singerView);
    }
}
