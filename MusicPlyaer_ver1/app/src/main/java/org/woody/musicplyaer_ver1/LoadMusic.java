package org.woody.musicplyaer_ver1;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by woody on 2016. 8. 9..
 */
public class LoadMusic extends RecyclerView.Adapter<ViewHolder> {


    private Context context;
    private ArrayList<Music> musicList;
    private LayoutInflater layoutInflater = null;

    public LoadMusic(Context context) {
        this.context = context;
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        musicList = new ArrayList<Music>();
        getMusicInfo();
    }

    public ArrayList<Music> getMusicList() {
        return musicList;
    }

    public void getMusicInfo() {
        String[] proj = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST
        };

        Cursor musicCursor = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                                            proj,null,null,null);
        if(musicCursor != null && musicCursor.moveToFirst()){
            String musicID;
            String albumID;
            String musicTitle;
            String singer;

            // index 가져오기
            int musicIDCol = musicCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int albumlDCol = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            int musicTitleCol = musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int singerCol = musicCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);

            do{

                musicID = musicCursor.getString(musicIDCol);
                albumID = musicCursor.getString(albumlDCol);
                musicTitle = musicCursor.getString(musicTitleCol);
                singer = musicCursor.getString(singerCol);

                Music music = new Music();
                String uri = getAlbumArt(Integer.parseInt(albumID)).toString();
                music.setAlbumUri(uri);

                Uri musicURI = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,""+musicID);

                music.setMusicUri(musicURI);
                music.setMusicTitle(musicTitle);
                music.setSinger(singer);
                musicList.add(music);

            }while (musicCursor.moveToNext());
        }
        musicCursor.close();
        return;
    }

    private Uri getAlbumArt(long albumId) {
        Uri artworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(artworkUri,albumId);
        return uri;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                                            .inflate(R.layout.item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        Music music_temp = musicList.get(position);
        holder.albumView.setImageURI(Uri.parse(music_temp.getAlbumUri()));
        holder.titleView.setText(music_temp.getMusicTitle());
        holder.singerView.setText(music_temp.getSinger());
    }

    @Override
    public int getItemCount() {
        return (musicList != null) ? musicList.size() : 0;
    }
}
